-- Adminer 4.2.4 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP DATABASE IF EXISTS `member`;
CREATE DATABASE `member` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `member`;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL,
  `password` varchar(15) NOT NULL,
  `name` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `username`, `password`, `name`, `lastname`) VALUES
(3,	'jack',	'1234',	'jack',	'hotelier'),
(4,	'root',	'1234',	'root',	'rootLastName'),
(5,	'aboy',	'1234',	'aboy',	'hotelierDemo'),
(6,	'micheal',	'1234',	'Micheal',	'Romanof');

DROP TABLE IF EXISTS `user_balance`;
CREATE TABLE `user_balance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `balance_amount` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `user_balance` (`id`, `user_id`, `balance_amount`) VALUES
(19,	3,	1200),
(20,	4,	5200),
(21,	5,	5000),
(22,	6,	400);

DROP TABLE IF EXISTS `user_bills`;
CREATE TABLE `user_bills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `price` float NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `is_paid` bit(1) NOT NULL DEFAULT b'0',
  `timestamp` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `user_bills` (`id`, `name`, `price`, `user_id`, `is_paid`, `timestamp`) VALUES
(4,	'Electricity',	2500,	3,	CONV('1', 2, 10) + 0,	'0000-00-00 00:00:00'),
(5,	'Rental Car',	1000,	3,	CONV('1', 2, 10) + 0,	'0000-00-00 00:00:00'),
(6,	'Water bill',	500,	3,	CONV('0', 2, 10) + 0,	'0000-00-00 00:00:00'),
(7,	'House rental',	4000,	3,	CONV('0', 2, 10) + 0,	'0000-00-00 00:00:00');

DROP TABLE IF EXISTS `user_friends`;
CREATE TABLE `user_friends` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `friend_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `friend_id` (`friend_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `user_friends` (`id`, `user_id`, `friend_id`) VALUES
(44,	5,	4),
(46,	6,	4),
(47,	6,	5),
(48,	3,	4),
(49,	3,	5),
(50,	22,	3),
(51,	22,	4),
(52,	22,	5),
(53,	22,	6);

DROP TABLE IF EXISTS `user_transactions`;
CREATE TABLE `user_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `data` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `user_transactions` (`id`, `user_id`, `data`, `timestamp`) VALUES
(9,	3,	'[{\"id\":\"5\",\"user_id\":\"3\",\"name\":\"Rental Car\",\"price\":\"1000\"},{\"id\":\"6\",\"user_id\":\"3\",\"name\":\"Water bill\",\"price\":\"500\"}]',	'2017-06-21 10:33:42'),
(10,	3,	'[{\"id\":\"4\",\"user_id\":\"3\",\"name\":\"Electricity\",\"price\":\"2500\"},{\"id\":\"5\",\"user_id\":\"3\",\"name\":\"Rental Car\",\"price\":\"1000\"}]',	'2017-06-22 02:17:29');

-- 2017-06-22 03:56:08
