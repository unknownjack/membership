/**
 * Created by mac on 6/15/17.
 */
(function(){
    function addEvent(elements, eventName, fn) {
        var objType = Object.prototype.toString.call(elements);
        if (objType === '[object NodeList]') {
            elements.forEach(function(el) {
                el.addEventListener(eventName, fn);
            });
        } else if (objType === '[object HTMLInputElement]') {
            elements.addEventListener(eventName, fn);
        }
    }

    var btnCheckAll = document.getElementById('check_all');
    var btnCheckBoxs = document.getElementsByName('selected_bill_ids[]');
    var totalCheckboxs = btnCheckBoxs.length;
    var totalChecked = 0;

    if (btnCheckAll && btnCheckBoxs && btnCheckBoxs.length > 0) {
        addEvent(btnCheckAll, 'change',function(ev) {
            var isChecked = ev.target.checked;

            if (isChecked) {
                totalChecked = 3;
            } else {
                totalChecked = 0;
            }

            btnCheckBoxs.forEach(function(checkBox) {
                checkBox.checked = isChecked;
            });
        });

        addEvent(btnCheckBoxs, 'change', function(ev) {
            var ckBox = ev.target;

            if (ckBox.checked) {
                totalChecked+=1;
            } else {
                totalChecked-=1;
            }

            if (totalChecked === totalCheckboxs) {
                btnCheckAll.checked = true;
            } else {
                btnCheckAll.checked = false;
            }
        });
    }
})();