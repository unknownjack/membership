<?php
//    require_once(BASEPATH.'core/Controller.php');
    /**
     * Created by PhpStorm.
     * User: mac
     * Date: 6/7/17
     * Time: 1:28 PM
     */
    Class BaseController extends CI_Controller {
        protected $pageData;
        protected $links;
        protected $tabLinks;

        public function __construct() {
            parent::__construct();
            $this->config->load('links', TRUE);
            $this->config->load('tabs', TRUE);
            $this->links = $this->config->item('links');
            $this->tabLinks = $this->config->item('tabs');
            $this->_verifyLoggedin();
        }

        private function _verifyLoggedin() {
            if (!$this->session->userdata('is_logged_in')) {
                if ($this->router->method != 'login' && $this->router->method != 'register') {
                    redirect($this->links['Login'], 'refresh');
                }
            } else {
                $this->AuthUser = unserialize($this->session->userdata('authenticated_user'));
            }
        }

        /**
         * @param       $path
         * @param       $data
         * @param bool  $includedNav
         * @param array $styleSheets
         * @param array $javaScripts
         */
        protected function _loadView($path, $data = [], $styleSheets = [], $javaScripts = [], $includedNav = TRUE) {
            $styleSheets = ['styleSheets' => $styleSheets];
            $javaScripts = ['javaScripts' => $javaScripts];
            $data = $this->_genViewData($data);

            $this->load->view('templates/header', array_merge($data, $styleSheets));
            if ($includedNav) {
                $this->load->view('templates/navbar', $data);
            }
            $this->load->view($path, array_merge($data, $javaScripts));
            $this->load->view('templates/footer');
        }

        /**
         * @param $data
         * @return array
         */
        protected function _genViewData($data) {
            $defaultPageData = [
                'pageTitle'   => 'Page Title',
                'links'       => $this->links,
                'tabLinks'    => $this->tabLinks,
                'AuthUser'    => $this->AuthUser,
                'styleSheets' => [],
                'javascripts' => [],
                'errors'      => []
            ];

            return array_merge($defaultPageData, $data);
        }
    }