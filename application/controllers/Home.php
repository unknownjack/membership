<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    require_once APPPATH . 'controllers/BaseController.php';

    class Home extends BaseController {
        public function index() {
            $this->load->helper('number');
            $userID = $this->AuthUser->getUserID();
            $balance = $this->BalanceModel->getBalanceAt($userID);
            $data = [
                'pageTitle'   => 'Home',
                'userBalance' => $balance
            ];

            $styleSheets = [
                'public/css/home.css'
            ];
            $this->_loadview('homes/home', $data, $styleSheets);
        }
    }
