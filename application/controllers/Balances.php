<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    require_once APPPATH . 'controllers/BaseController.php';

    class Balances extends BaseController {
        private $userID;
        private $currentBalance;

        public function __construct() {
            parent::__construct();
            $this->userID = $this->AuthUser->getUserID();
            $this->currentBalance = $this->BalanceModel->getBalanceAt($this->userID);
        }

        public function transaction() {
            $data = [
                'pageTitle'      => 'Wallet',
                'currentBalance' => $this->currentBalance
            ];
            $transactionType = $this->input->post('submit');

            if ($transactionType == 'deposit' || $transactionType == 'withdraw') {
                $this->load->library('form_validation');

                if ($this->form_validation->run('transaction_' . $transactionType)) {
                    $transactionAmount = $this->input->post('transaction_amount');
                    $newBalance = $this->_getNewBalance($transactionType, $transactionAmount);

                    if ($this->BalanceModel->updateBalanceAt($this->userID, $newBalance)) {
                        redirect(base_url($this->links['transaction']), 'location');
                    } else {
                        $data['errors'] = [
                            'transaction' => 'There was an error while we update your balance.'
                        ];
                    }
                } else {
                    $data['errors'] = $this->form_validation->error_array();
                }
            }

            $this->_loadview('balances/wallet', $data);
        }

        /**
         * @param int $userID the user's id to transfer money to
         */
        public function transfers($userID = 0) {
            $selectedFriendID = $userID;
            $data = array(
                'pageTitle'        => 'Transfers',
                'currentBalance'   => $this->currentBalance,
                'users'            => array(),
                'selectedFriendID' => $selectedFriendID
            );

            if ($this->input->post('submit')) {
                $this->load->library('form_validation');

                if ($this->form_validation->run('transfers')) {
                    $this->load->library('databaseutils');
                    $transferAmount = $this->input->post('transfers_amount');
                    $transferTo = $this->input->post('transfers_to');

                    if ($this->databaseutils->runTransaction($this, '_processTransfer', [$transferTo, $transferAmount])) {
                        redirect(base_url($this->links['transfers']), 'location');
                    } else {
                        $data['errors'] = array(
                            'transfers' => 'There is an error while we are transferring your money.'
                        );
                    }
                } else {
                    $data['errors'] = $this->form_validation->error_array();
                }
            }

            $users = $this->UserModel->getAllUserWithOut($this->userID);
            foreach ($users as $user) {
                $data['users'][$user['id']] = $user['name'] . ' ' . $user['lastname'];
            }

            $this->_loadview('balances/transfers', $data);
        }

        /**
         * Function to calculate new Balance
         *
         * @param $type Transaction's type (deposit/withdraw)
         * @param $amount Transaction's amount
         * @return float New Balance
         */
        private function _getNewBalance($type, $amount) {
            if ($type == 'deposit') {
                return $this->currentBalance + $amount;
            } else if ($type == 'withdraw') {
                return $this->currentBalance - $amount;
            } else {
                return $this->currentBalance;
            }
        }

        /**
         * Validation callback method for transfers_amount field
         *
         * @param $amount money that will be transferred
         * @return bool
         */
        public function _withdraw_constraint($amount) {
            if ($amount > $this->currentBalance) {
                return FALSE;
            } else {
                return TRUE;
            }
        }

        /**
         * Validation callback method for transfers_amount field
         * @param $transfersAmount
         * @return bool
         */
        public function _transfers_amount($transfersAmount) {
            if ($transfersAmount > $this->currentBalance) {
                return false;
            } else {
                return true;
            }
        }

        public function _processTransfer($transferTo, $amount) {
            $receiveOldBalance = $this->BalanceModel->getBalanceAt($transferTo);
            $receiveNewBalance = $receiveOldBalance + $amount;
            $this->BalanceModel->updateBalanceAt($transferTo, $receiveNewBalance);

            $senderOldBalance = $this->BalanceModel->getBalanceAt($this->userID);
            $senderNewBalance = $senderOldBalance - $amount;
            $this->BalanceModel->updateBalanceAt($this->userID, $senderNewBalance);
        }

    }