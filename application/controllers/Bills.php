<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    require_once APPPATH . 'controllers/BaseController.php';

    /**
     * Created by PhpStorm.
     * User: mac
     * Date: 6/14/17
     * Time: 4:31 PM
     */
    class Bills extends BaseController {
        private $userId;
        private $currentBalance;

        public function __construct() {
            parent::__construct();
            $this->userId = $this->AuthUser->getUserID();
            $this->currentBalance = $this->BalanceModel->getBalanceAt($this->userId);
            $this->load->library('BillUtils');
        }

        public function index() {
            $this->pay();
        }

        public function pay() {
            $this->load->library('form_validation');
            $data = [
                'pageTitle'      => 'Bills',
                'bills'          => [],
                'currentBalance' => $this->currentBalance,
                'totalPrice'     => 0
            ];

            if ($this->input->post('submit') == 'next') {
                if ($this->form_validation->run('prePayBill')) {
                    $this->_showConfirmationPage($data);
                } else {
                    $data['errors'] = $this->form_validation->error_array();
                    $this->_showIndexPage($data);
                }
            } elseif ($this->input->post('submit') == 'pay') {
                if ($this->form_validation->run('payBill')) {
                    $selectedBillIDs = $this->input->post('selected_bill_ids');
                    $price = $this->input->post('price');

                    $payStatus = $this->_isPaySuccess($selectedBillIDs, $price, $data);
                    $data = $payStatus['data'];

                    if ($payStatus['isSuccess']) {
                        $this->_showSummaryPage($data);
                    } else {
                        $this->_showConfirmationPage($data);
                    }
                } else {
                    $data['errors'] = $this->form_validation->error_array();
                    $this->_showConfirmationPage($data);
                }
            } else {
                $this->_showIndexPage($data);
            }
        }

        /**
         * @param array $selectedBillIDs
         * @param int   $price
         * @param array $data
         * @return array
         */
        private function _isPaySuccess($selectedBillIDs = [], $price = 0, $data = []) {
            $newBalance = $this->currentBalance - $price;

            if ($this->BalanceModel->updateBalanceAt($this->userId, $newBalance)) {
                if ($this->BillModel->updateStatusBillsIn($selectedBillIDs, TRUE)) {
                    $data['currentBalance'] = $newBalance;
                    $isSuccess = TRUE;
                    $transactionID = $this->billutils->saveTransaction($this->userId, $selectedBillIDs);
                    redirect($this->links['PaySummary'] . '/' . $transactionID, 'location');
                } else {
                    $this->BalanceMoDel->updateBalanceAt($this->userId, $this->currentBalance);
                    $data['errors'] = [
                        'paying' => 'There is an error while doing this transaction.'
                    ];
                    $isSuccess = FALSE;
                }
            } else {
                $data['errors'] = [
                    'paying' => 'There is an error while doing this transaction.'
                ];
                $isSuccess = FALSE;
            }

            return [
                'isSuccess' => $isSuccess,
                'data'      => $data
            ];
        }

        /**
         * @param array $data
         */
        private function _showIndexPage($data = []) {
            $data['pageTitle'] = 'Bills';
            $bills = $this->BillModel->getAllBillNotPaidOf($this->userId);
            $data['bills'] = $bills;
            $data['totalPrice'] = $this->billutils->sumBillsPrice($bills);
            $javaScripts = ['public/js/bill.js'];

            $this->_loadview('bills/bill', $data,  NULL, $javaScripts);
        }

        /**
         * @param array $data
         */
        private function _showConfirmationPage($data = []) {
            $selectedBillIDs = $this->input->post('selected_bill_ids');

            $data['pageTitle'] = 'Bills Confirmation';
            $bills = $this->BillModel->getAllBillsIn($selectedBillIDs);
            $data['bills'] = $bills;
            $data['totalPrice'] = $this->billutils->sumBillsPrice($bills);

            $this->_loadview('bills/bill_confirm', $data);
        }

        public function summary($transactionID = 0) {
            $this->load->model('TransactionModel');

            $data = [
                'pageTitle'      => 'Bill Summary',
                'bills'          => [],
                'currentBalance' => $this->currentBalance,
                'totalPrice'     => 0
            ];
            $transaction = $this->TransactionModel->getTransactionAt($transactionID);
            if (sizeof($transaction) > 0) {
                $bills = json_decode($transaction[0]['data'], TRUE);
            } else {
                $bills = [];
            }
            $data['bills'] = $bills;
            $data['totalPrice'] = $this->billutils->sumBillsPrice($bills);

            $this->_loadview('bills/bill_summary', $data);
        }

        /**
         * @param int $price
         * @return bool
         */
        public function _validate_price($price = 0) {
            if ($price > $this->currentBalance) {
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }