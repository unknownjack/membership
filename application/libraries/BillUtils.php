<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 6/16/17
 * Time: 4:03 PM
 */
class BillUtils {
    private $CI;

    public function __construct() {
        $this->CI =& get_instance();
    }

    /**
     * @param $bills
     * @return int
     */
    public function sumBillsPrice($bills) {
        $sum = 0;
        foreach ($bills as $bill) {
            $sum += $bill['price'];
        }
        return $sum;
    }

    /**
     * @param $userID
     * @param array $billIDs
     * @return mixed Transaction_id
     */
    public function saveTransaction($userID, $billIDs = array()) {
        $this->CI->load->model('BillModel');
        $this->CI->load->model('TransactionModel');

        $bills = $this->CI->BillModel->getAllBillsIn($billIDs);
        $dataEncoded = $this->generateBillsEncoded($bills);
        return $this->CI->TransactionModel->insertTransaction($userID, $dataEncoded);
    }

    /**
     * @param array $bills
     * @return string
     */
    private function generateBillsEncoded($bills = array()) {
        $billCollection = array();
        foreach ($bills as $bill) {
            $billCollection[] = array(
                BillModel::$id     => $bill[BillModel::$id],
                BillModel::$userID => $bill[BillModel::$userID],
                BillModel::$name   => $bill[BillModel::$name],
                BillModel::$price  => $bill[BillModel::$price]
            );
        }
        return json_encode($billCollection);
    }
}