<?php

    /**
     * Created by PhpStorm.
     * User: mac
     * Date: 6/21/17
     * Time: 2:27 PM
     */
    class CustomLoader {
        public function loadCustomBaseClass() {
            $CFG =& load_class('Config', 'core');

            if (file_exists(APPPATH . 'Controllers/' . $CFG->config['subclass_prefix'] . 'Controller.php')) {
                require_once APPPATH . 'Controllers/' . $CFG->config['subclass_prefix'] . 'Controller.php';
            }

            if (file_exists(APPPATH . 'models/' . $CFG->config['subclass_prefix'] . 'Model.php')) {
                require_once APPPATH . 'models/' . $CFG->config['subclass_prefix'] . 'Model.php';
            }
        }
    }