<div class="container">
    <div class="panel panel-default">
        <div class="panel-body">
            <form method="POST" action="<?= base_url($links['transfers']); ?>">
                <?php if (isset($errors['transfers'])) { ?>
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Error!</strong> <?= $errors['transfers']; ?>
                    </div>
                <?php } ?>
                <div class="well well-lg">Your current Balance is <strong><?= $currentBalance ?></strong> Baht</div>
                <div class="form-group <?= (isset($errors['transfers_to'])) ? 'has-error':''; ?>">
                    <label for="transfers_to">Transfer to</label>
                    <?= form_dropdown('transfers_to', $users, $selectedFriendID, array('id'=>'transfers_to', 'class'=>'form-control')); ?>
                    <?= form_error('transfers_to', '<span class="help-block">', '</span>'); ?>
                </div>
                <div class="form-group <?= (isset($errors['transfers_amount'])) ? 'has-error':''; ?>">
                    <label for="transfers_amount">Transfer amount</label>
                    <input type="text" class="form-control" name="transfers_amount" id="transfers_amount" placeholder="">
                    <?= form_error('transfers_amount', '<span class="help-block">', '</span>'); ?>
                </div>
                <button type="submit" class="btn btn-primary" name="submit" value="transfer">Transfers</button>
            </form>
        </div>
    </div>
</div>