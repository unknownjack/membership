<div class="container">
    <div class="page-header">
        <h1>Bills Summary</h1>
    </div>
    <div class="well well-lg">Your current Balance is <strong><?= $currentBalance ?></strong> Baht</div>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Success!</strong> Your bills have been successfully paid.
    </div>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Price</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($bills as $key => $bill) { ?>
            <tr>
                <td><?= $key + 1 ?></td>
                <td><?= $bill['name'] ?></td>
                <td><?= $bill['price'] ?></td>
            </tr>
        <?php } ?>
        </tbody>
        <tfoot>
        <tr>
            <th></th>
            <th class="text-right">Total Price:</th>
            <th><?= $totalPrice ?></th>
        </tr>
        </tfoot>
    </table>
</div>