<div class="container">
    <div class="page-header">
        <h1>Bills Confirmation</h1>
    </div>
    <form action="<?= base_url($links['PayBill']); ?>" method="post">
        <div class="well well-lg">Your current Balance is <strong><?= $currentBalance ?></strong> Baht</div>
        <?php if (isset($errors['price'])) { ?>
            <div class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Warning!</strong> <?= $errors['price']; ?>
            </div>
        <?php } ?>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Price</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($bills as $key => $bill) { ?>
                <tr>
                    <td><?= $key + 1 ?></td>
                    <td><?= $bill['name'] ?></td>
                    <td><?= $bill['price'] ?></td>
                    <input type="hidden" name="selected_bill_ids[]" value="<?= $bill['id'] ?>"/>
                </tr>
            <?php } ?>
            </tbody>
            <tfoot>
            <tr>
                <th></th>
                <th class="text-right">Total Price:</th>
                <th><?= $totalPrice ?></th>
            </tr>
            </tfoot>
        </table>
        <input type="hidden" name="price" value="<?= $totalPrice ?>">
        <button class="btn btn-lg btn-primary pull-right" type="submit" name="submit" value="pay">Pay</button>
        <button class="btn btn-lg btn-default" type="button" onclick="window.location.href='<?= base_url($links['Bills']); ?>'">Cancel</button>
    </form>
</div>