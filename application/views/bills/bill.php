<div class="container">
    <div class="page-header">
        <h1>Bills</h1>
    </div>
    <form action="<?= base_url($links['PayBill']); ?>" method="post">
        <div class="well well-lg">Your current Balance is <strong><?= $currentBalance ?></strong> Baht</div>
        <?php if (isset($errors['selected_bill_ids[]'])) { ?>
            <div class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Warning!</strong> <?= $errors['selected_bill_ids[]']; ?>
            </div>
        <?php } ?>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>
                        <input type="checkbox" id="check_all"/>
                    </th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($bills as $key => $bill) { ?>
                <tr>
                    <td>
                        <input type="checkbox" name="selected_bill_ids[]" value="<?= $bill['id'] ?>"/>
                    </td>
                    <td><?= $bill['name'] ?></td>
                    <td><?= $bill['price'] ?></td>
                    <td><?= $bill['is_paid'] ? 'Paid':'Not Paid' ?></td>
                </tr>
            <?php } ?>
            </tbody>
            <tfoot>
                <tr>
                    <th></th>
                    <th class="text-right">Total Price:</th>
                    <th><?= $totalPrice ?></th>
                    <th></th>
                </tr>
            </tfoot>
        </table>
        <button class="btn btn-lg btn-primary pull-right" type="submit" name="submit" value="next">Next</button>
    </form>
</div>