<?php
    /**
     * Created by PhpStorm.
     * User: mac
     * Date: 6/14/17
     * Time: 4:45 PM
     */
    require_once APPPATH . 'models/BaseModel.php';

    class BillModel extends BaseModel {
        static $tableName = 'user_bills';
        static $id = 'id';
        static $name = 'name';
        static $price = 'price';
        static $userID = 'user_id';
        static $isPaid = 'is_paid';

        /**
         * @param $userID
         * @return mixed
         */
        public function getAllBillOf($userID) {
            $this->db->where(BillModel::$userID, $userID);
            $query = $this->db->get(BillModel::$tableName);

            return $query->result_array();
        }

        public function getAllBillNotPaidOf($userID) {
            $where = [
                BillModel::$userID        => $userID,
                BillModel::$isPaid . '!=' => TRUE
            ];

            $this->db->where($where);
            $query = $this->db->get(BillModel::$tableName);

            return $query->result_array();
        }

        /**
         * @param $billIDs
         * @return mixed
         */
        public function getAllBillsIn($billIDs) {
            $this->db->where_in(BillModel::$id, $billIDs);
            $query = $this->db->get(BillModel::$tableName);

            return $query->result_array();
        }

        /**
         * @param $billIDs
         * @param $isPaid
         * @return mixed
         */
        public function updateStatusBillsIn($billIDs, $isPaid) {
            $data = [
                BillModel::$isPaid => $isPaid
            ];
            $this->db->where_in(BillModel::$id, $billIDs);

            return $this->db->update(BillModel::$tableName, $data);
        }
    }