<?php
    require_once APPPATH . 'models/BaseModel.php';

    /**
     * Created by PhpStorm.
     * User: mac
     * Date: 6/16/17
     * Time: 2:40 PM
     */
    class TransactionModel extends BaseModel {
        static $tableName = 'user_transactions';
        static $id = 'id';
        static $userID = 'user_id';
        static $data = 'data';

        /**
         * @param $userID (int)
         * @param $data   (json_encoded)
         * @return transaction_id (int)
         */
        public function insertTransaction($userID, $data) {
            $data = [
                TransactionModel::$userID => $userID,
                TransactionModel::$data   => $data
            ];

            $this->db->insert(TransactionModel::$tableName, $data);

            return $this->db->insert_id();
        }

        /**
         * @param int $transactionID
         * @return mixed
         */
        public function getTransactionAt($transactionID = 0) {
            $this->db->where(TransactionModel::$id, $transactionID);
            $query = $this->db->get(TransactionModel::$tableName);

            return $query->result_array();
        }
    }